package nl.tjonahen.ccdp.exercises.thedoors;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GarageDoorTest {


    @Test
    void openTheDoor() {
        GarageDoor door = new GarageDoor();

        door.receiveRemoteSignal();
        door.waitForDoorToComplete();
        assertTrue(door.isOpen());
    }

    @Test
    void closeTheDoor() {
        GarageDoor door = new GarageDoor();

        door.receiveRemoteSignal();
        door.waitForDoorToComplete();
        assertTrue(door.isOpen());

        door.receiveRemoteSignal();
        door.waitForDoorToComplete();

        assertFalse(door.isOpen());
    }

}