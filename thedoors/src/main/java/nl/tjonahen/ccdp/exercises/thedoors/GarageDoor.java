package nl.tjonahen.ccdp.exercises.thedoors;

public class GarageDoor {
    private enum State {
        OPEN, CLOSE, OPENING, CLOSING
    }
    private State state = State.CLOSE;

    public void receiveRemoteSignal() {
        if (isOpen()) {
            switch (state) {
                case OPEN -> keepTheDoorOpen();
                case CLOSE -> startOpeningTheDoor();
                case OPENING -> keepOpeningTheDoor();
                case CLOSING -> startOpeningTheDoor();
            }
        } else {
            switch (state) {
                case OPEN -> startClosingTheDoor();
                case CLOSE -> keepTheDoorClose();
                case OPENING -> startClosingTheDoor();
                case CLOSING -> keepClosingTheDoor();
            }
        }
    }
    public void waitForDoorToComplete() {
        switch (state) {
            case OPEN -> keepTheDoorOpen();
            case CLOSE -> keepTheDoorClose();
            case OPENING -> doneOpeningTheDoor();
            case CLOSING -> doneClosingTheDoor();
        }
    }
    public boolean isOpen() {
        return state == State.OPEN;
    }

    private void keepClosingTheDoor() {
        // TODO: Implement continue closing the door logic here.
    }

    private void keepTheDoorClose() {
        // TODO: Implement keep the door closed logic here.
    }

    private void startClosingTheDoor() {
        // TODO: Implement start closing the door logic here.
        //  remove the hook from the door and start closing it
        state = State.CLOSING;
    }

    private void keepOpeningTheDoor() {
        // TODO: Implement continue closing the door logic here.
    }

    private void startOpeningTheDoor() {
        // TODO: Implement start opening the door logic here.
        //  unlock the door and open it.
        state = State.OPENING;
    }

    private void keepTheDoorOpen() {
        // TODO: Implement keep the door open logic here.
    }

    private void doneClosingTheDoor() {
        // TODO: Implement finalize closing the door logic here.
        //  lock the door?
        state = State.CLOSE;
    }

    private void doneOpeningTheDoor() {
        // TODO: Implement finalize opening the door logic here.
        //  put the hook on the door?
        state = State.OPEN;
    }
}
