package nl.tjonahen.ccdp.exercises;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RenderEngineTest {

    @Test
    void plain() {
        assertEquals("Hello, world!", new RenderEngine(new Plain()).printer());
    }

    @Test
    void Strong() {
        assertEquals("<strong>Hello, world!</strong>",
                new RenderEngine(new Strong()).printer());
    }

    @Test
    void div() {
        assertEquals("""
                                <div>
                                <strong>Hello, world!</strong>
                                </div>""",
                new RenderEngine(new Div()).printer());
    }

    @Test
    void head() {
        assertEquals("""
                                <head><title>Jo, dummy</title></head>
                                <body>
                                <div>
                                <strong>Hello, world!</strong>
                                </div>
                                </body>""",
                new RenderEngine(new Head()).printer());
    }

    @Test
    void body() {
        assertEquals("""
                                <body>
                                <div>
                                <strong>Hello, world!</strong>
                                </div>
                                </body>""",
                new RenderEngine(new Body()).printer());
    }

    @Test
    void html() {
        assertEquals("""
                                <html>
                                <head><title>Jo, dummy</title></head>
                                <body>
                                <div>
                                <strong>Hello, world!</strong>
                                </div>
                                </body>
                                </html>""",
                new RenderEngine(new Html()).printer());
    }

    @Test
    void blink() {
        assertEquals("<blink>Hello, world!</blink>", new RenderEngine(new Blink()).printer());
    }


}