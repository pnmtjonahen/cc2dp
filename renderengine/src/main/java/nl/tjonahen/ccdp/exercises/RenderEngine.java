package nl.tjonahen.ccdp.exercises;

public class RenderEngine {
    private final Markup printer;

    public RenderEngine(Markup printer) {
        this.printer = printer;
    }

    public String printer() {
        return printer.addTags("Hello, world!");
    }
}

interface Markup {
    String addTags(String input);
}

class Plain implements Markup {
    public String addTags(String input) {
        return input;
    }
}

class Strong extends Plain {
    @Override
    public String addTags(String input) {
        return String.format("<strong>%s</strong>", super.addTags(input));
    }
}

class Blink extends Plain {
    @Override
    public String addTags(String input) {
        return String.format("<blink>%s</blink>", super.addTags(input));
    }
}

class Div extends Strong {
    @Override
    public String addTags(String input) {
        return String.format("<div>%n%s%n</div>", super.addTags(input));
    }
}

class Body extends Div {
    @Override
    public String addTags(String input) {
        return String.format("<body>%n%s%n</body>", super.addTags(input));
    }
}

class Head extends Body {
    @Override
    public String addTags(String input) {
        return String.format("<head><title>Jo, dummy</title></head>%n%s", super.addTags(input));
    }

}

class Html extends Head {
    @Override
    public String addTags(String input) {
        return String.format("<html>%n%s%n</html>", super.addTags(input));
    }
}
