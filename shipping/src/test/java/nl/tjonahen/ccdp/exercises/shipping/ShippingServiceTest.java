package nl.tjonahen.ccdp.exercises.shipping;

import nl.tjonahen.ccdp.exercises.shipping.model.Address;
import nl.tjonahen.ccdp.exercises.shipping.model.Customer;
import nl.tjonahen.ccdp.exercises.shipping.transporters.DomesticTransport;
import nl.tjonahen.ccdp.exercises.shipping.transporters.ElectronicTransport;
import nl.tjonahen.ccdp.exercises.shipping.transporters.InternationalTransport;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNull;

class ShippingServiceTest {

    @BeforeEach
    void setup() {
        System.setProperty("perform.action", "true");
    }

    @AfterEach
    void tearDown() {
        System.clearProperty("perform.action");
    }

    @Test
    void test1() {
        Transporter state = new Transporter();
        ShippingService shippingService = new ShippingService(state);

        Customer customer = new Customer();
        customer.setHomeAddress(new Address());
        shippingService.shipToCustomer(customer);

        assertInstanceOf(InternationalTransport.class, state.getTransportBy());
    }

    @Test
    void test1a() {
        Transporter state = new Transporter();
        ShippingService shippingService = new ShippingService(state);

        Customer customer = new Customer();
        Address homeAddress = new Address();
        homeAddress.setCountry("Nederland");
        customer.setHomeAddress(homeAddress);
        shippingService.shipToCustomer(customer);

        assertInstanceOf(DomesticTransport.class, state.getTransportBy());
    }

    @Test
    void test2() {
        Transporter state = new Transporter();
        ShippingService shippingService = new ShippingService(state);

        Customer customer = new Customer();
        customer.setEmailAddress("test@home.nl");
        shippingService.shipToCustomer(customer);

        assertInstanceOf(ElectronicTransport.class, state.getTransportBy());

    }

    @Test
    void test3() {
        Transporter state = new Transporter();
        ShippingService shippingService = new ShippingService(state);

        Customer customer = new Customer();
        customer.setShippingAddres(new Address());
        shippingService.shipToCustomer(customer);

        assertInstanceOf(InternationalTransport.class, state.getTransportBy());

    }

    @Test
    void test3a() {
        Transporter state = new Transporter();
        ShippingService shippingService = new ShippingService(state);

        Customer customer = new Customer();
        Address shippingAddress = new Address();
        shippingAddress.setCountry("Nederland");
        customer.setShippingAddres(shippingAddress);
        shippingService.shipToCustomer(customer);

        assertInstanceOf(DomesticTransport.class, state.getTransportBy());

    }

    @Test
    void test4() {
        Transporter state = new Transporter();
        ShippingService shippingService = new ShippingService(state);

        Customer customer = new Customer();
        customer.setOfficeAddress(new Address());
        shippingService.shipToCustomer(customer);

        assertNull(state.getTransportBy());

    }

}