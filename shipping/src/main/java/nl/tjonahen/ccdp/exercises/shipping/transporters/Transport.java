package nl.tjonahen.ccdp.exercises.shipping.transporters;

public interface Transport<T> {

    T getAddress();
}
