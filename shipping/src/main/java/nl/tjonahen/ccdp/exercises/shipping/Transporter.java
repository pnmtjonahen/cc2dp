package nl.tjonahen.ccdp.exercises.shipping;

import nl.tjonahen.ccdp.exercises.shipping.transporters.Transport;

public class Transporter {

    private Transport transportBy;

    public Transport getTransportBy() {
        return transportBy;
    }

    public void setTransportBy(Transport transportBy) {
        this.transportBy = transportBy;
    }
}
