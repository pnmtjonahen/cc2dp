package nl.tjonahen.ccdp.exercises.shipping;

import nl.tjonahen.ccdp.exercises.shipping.transporters.InternationalTransport;
import nl.tjonahen.ccdp.exercises.shipping.model.Customer;
import nl.tjonahen.ccdp.exercises.shipping.transporters.DomesticTransport;
import nl.tjonahen.ccdp.exercises.shipping.transporters.ElectronicTransport;

public class ShippingService {

    public static final String DOMESTIC = "Nederland";
    private final Transporter transporter;

    public ShippingService(Transporter transporter) {
        this.transporter = transporter;
    }

    public void shipToCustomer(Customer customer) {

        if (customer.getHomeAddress() != null && DOMESTIC.equals(customer.getHomeAddress().getCountry())) {
            // handle ship to customer home address
            transporter.setTransportBy(new DomesticTransport(customer.getHomeAddress()));
        }

        if (customer.getHomeAddress() != null && !DOMESTIC.equals(customer.getHomeAddress().getCountry())) {
            // handle ship to customer international home address
            transporter.setTransportBy(new InternationalTransport(customer.getHomeAddress()));
        }

        // ship to customers shipping address
        if (customer.getShippingAddres() != null && DOMESTIC.equals(customer.getShippingAddres().getCountry())) {
            // handle ship to customer shipping address
            transporter.setTransportBy(new DomesticTransport(customer.getShippingAddres()));
        }

        if (customer.getShippingAddres() != null && !DOMESTIC.equals(customer.getShippingAddres().getCountry())) {
            // handle ship to customer international shipping address
            transporter.setTransportBy(new InternationalTransport(customer.getShippingAddres()));
        }

        // send via email
        if (customer.getEmailAddress() != null && !customer.getEmailAddress().isEmpty()) {
            // handle sending to email address
            transporter.setTransportBy(new ElectronicTransport(customer.getEmailAddress()));
        }

        if (customer.getOfficeAddress() != null) {
            // handle not supported shipping address
            System.out.println("Shipping to an office address not supported");
        }

    }

}

