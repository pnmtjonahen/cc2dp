package nl.tjonahen.ccdp.exercises.shipping.transporters;

import nl.tjonahen.ccdp.exercises.shipping.model.Address;

public class InternationalTransport implements Transport<Address> {
    private final Address shippingAddress;

    public InternationalTransport(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    @Override
    public Address getAddress() {
        return shippingAddress;
    }
}
