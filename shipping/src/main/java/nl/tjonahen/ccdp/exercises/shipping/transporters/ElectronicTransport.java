package nl.tjonahen.ccdp.exercises.shipping.transporters;

public class ElectronicTransport  implements Transport<String> {
    private final String emailAdress;

    public ElectronicTransport(String emailAdress) {
        this.emailAdress = emailAdress;
    }

    @Override
    public String getAddress() {
        return emailAdress;
    }
}
