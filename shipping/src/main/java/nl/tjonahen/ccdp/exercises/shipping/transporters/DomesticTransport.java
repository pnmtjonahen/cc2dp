package nl.tjonahen.ccdp.exercises.shipping.transporters;

import nl.tjonahen.ccdp.exercises.shipping.model.Address;

public class DomesticTransport implements Transport<Address> {
    private final Address shippingAddress;

    public DomesticTransport(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    @Override
    public Address getAddress() {
        return shippingAddress;
    }
}
