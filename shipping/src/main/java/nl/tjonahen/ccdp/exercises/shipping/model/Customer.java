package nl.tjonahen.ccdp.exercises.shipping.model;

public class Customer {

    private Address homeAddress;
    private Address officeAddress;
    private Address shippingAddres;
    private String emailAddress;

    public Address getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(Address homeAddress) {
        this.homeAddress = homeAddress;
    }

    public Address getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(Address officeAddress) {
        this.officeAddress = officeAddress;
    }

    public Address getShippingAddres() {
        return shippingAddres;
    }

    public void setShippingAddres(Address shippingAddres) {
        this.shippingAddres = shippingAddres;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
