package nl.tjonahen.ccdp;

import java.util.Date;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    @Test
    void onlyFirstName() {
        Person p = new Person("John");

        assertNotNull(p.getFirstName());
        assertNull(p.getMiddleName());
        assertNull(p.getLastName());
        assertNull(p.getBirthDate());
        assertNull(p.getNationality());
    }

    @Test
    void firstNameAndMiddleName() {
        Person p = new Person("John", "T");

        assertNotNull(p.getFirstName());
        assertNotNull(p.getMiddleName());
        assertNull(p.getLastName());
        assertNull(p.getBirthDate());
        assertNull(p.getNationality());
    }

    @Test
    void fullName() {
        Person p = new Person("John", "T", "Doe");

        assertNotNull(p.getFirstName());
        assertNotNull(p.getMiddleName());
        assertNotNull(p.getLastName());
        assertNull(p.getBirthDate());
        assertNull(p.getNationality());
    }
    @Test
    void withBirtDate() {
        Person p = new Person("John", "T", "Doe", new Date());

        assertNotNull(p.getFirstName());
        assertNotNull(p.getMiddleName());
        assertNotNull(p.getLastName());
        assertNotNull(p.getBirthDate());
        assertNull(p.getNationality());
    }

    @Test
    void withBirtDateNoMiddleName() {
        Person p = new Person("John", "Doe", new Date());

        assertNotNull(p.getFirstName());
        assertNull(p.getMiddleName());
        assertNotNull(p.getLastName());
        assertNotNull(p.getBirthDate());
        assertNull(p.getNationality());
    }

    @Test
    void withNationality() {
        Person p = new Person("John", "T", "Doe", new Date(), "Timboektoe");

        assertNotNull(p.getFirstName());
        assertNotNull(p.getMiddleName());
        assertNotNull(p.getLastName());
        assertNotNull(p.getBirthDate());
        assertNotNull(p.getNationality());
    }

    @Test
    void withNationalityNoMiddleName() {
        Person p = new Person("John", "Doe", new Date(), "Timboektoe");

        assertNotNull(p.getFirstName());
        assertNull(p.getMiddleName());
        assertNotNull(p.getLastName());
        assertNotNull(p.getBirthDate());
        assertNotNull(p.getNationality());
    }
    @Test
    void noName() {
        Person p = new Person(new Date(), "Timboektoe");

        assertNull(p.getFirstName());
        assertNull(p.getMiddleName());
        assertNull(p.getLastName());
        assertNotNull(p.getBirthDate());
        assertNotNull(p.getNationality());
    }

}