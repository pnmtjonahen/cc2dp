# What is what

## intro
Uitleg over wat in welke module zit en wat je er mee wilt. Dus wat het probleem is, en wat de oplossingsrichting is.

## 1. engineoverload
#### Wat doet het:
Constructor overloading met alle mogelijk combinaties van de verschillende Person variaties.

#### Oplossingsrichting:
- Builder pattern

Door een builder pattern toe te passen is er meer flexibility om verschillende verschijningsvormen van een persoon te creëren.

## 2. renderengine
#### Wat doet het:
Render een html-pagina, die is opgebouwd uit verschillende elementen, die een hiërarchische structuur vormt.

#### Oplossingsrichting:
- Inheritance vs Composition
- Interface vs Implementation
- Decorator pattern

Gebruik makend van een decorator pattern kan eenzelfde hiërarchische structuur gecreëerd worden.

## 3. shipping
#### Wat doet het:
Maakt het mogelijk om goederen te versturen via verschillende shipping methodes. Afhankelijk van het ingevuld adres wordt er een andere methode gekozen.

#### Oplossingsrichting:
- Strategy pattern

Kies een ander shipping strategy gebaseerd op het ingevuld shipping adres.

## 4. shoppingspree
#### Wat doet het:
Klanten kopen producten bij een winkel. Als iets niet meer voorradig is moeten ze later terug komen om te kijken of het er nu wel is. Beter is het om de klant te notificeren als een product weer aanwezig is.

#### Oplossingsrichting:
- Observer pattern

Gebruik het observer pattern om klanten te notificeren als de winkel bevoorraad wordt.

## 5. thedoors
#### Wat doet het:
Afhankelijk van de staat van de deur (Open, Dicht, word-gesloten of word-geopen) reageert de deur anders. 
#### Oplossingsrichting:
- State pattern

Afhankelijk van de staat gaat de deur over naar een andere staat, bij dezelfde input.

# Brainstorm

Welke opdrachten willen we nog meer:
- Adapter
- Factory/Abstract factory
- Singleton (Behandelen/melden hoeft geen voorbeeld nodig)
- Iterator (melden)
- Facade?


