package nl.tjonahen.ccdp.exercises.shoppingspree;

import java.util.Random;

/**
 * The different products than can be stocked at the supermarket.
 */
public enum Product {

    COFFEE,
    THEE,
    MILK,
    SUGAR,
    EGGS;

    private static final Random PRNG = new Random();

    public static Product randomProduct()  {
        Product[] directions = values();
        return directions[PRNG.nextInt(directions.length)];
    }
}
