package nl.tjonahen.ccdp.exercises.shoppingspree;

import java.util.ArrayList;
import java.util.List;

/**
 * The supermarket with products in stock.
 */
public class Supermarket {

    private final List<Product> stock = new ArrayList<>();

    /**
     * Check if a product is in stock.
     *
     * @param product the product the customer is interested in.
     * @return true if product is in stock, else false.
     */
    public boolean shop(Product product) {
        return stock.contains(product);
    }

    /**
     * Update the stock for a specific product.
     *
     * @param product the product that is delivered.
     */
    public void delivery(Product product) {
        System.out.println("Supermarket: Received product " + product + " update stock");
        stock.add(product);
    }
}
