package nl.tjonahen.ccdp.exercises.shoppingspree;

/**
 * The customer (aka the consumer of products)
 *
 * The customer goes shopping at the supermarket until the product is available or it is told to stop shopping.
 *
 */
public class Customer {
    private final String name;
    private final Product product;
    private boolean happy = false;
    private boolean continueShopping = true;

    public Customer(String name, Product product) {
        this.name = name;
        this.product = product;
    }

    void goShopping(Supermarket supermarket) {
        while (!happy && continueShopping) {
            happy = supermarket.shop(product);
            System.out.println("Customer: " + name + " " + product + " not available");
        }
    }

    public void stopShopping() {
        continueShopping = false;
    }

    public String getName() {
        return name;
    }

    public boolean isHappy() {
        return happy ;
    }
}
