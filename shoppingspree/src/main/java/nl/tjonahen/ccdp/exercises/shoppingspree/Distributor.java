package nl.tjonahen.ccdp.exercises.shoppingspree;

/**
 * The distributor of products, (aka a producer of products)
 *
 */
public class Distributor {
    private final Supermarket supermarket;

    public Distributor(Supermarket supermarket) {
        this.supermarket = supermarket;
    }

    public void distributeAllProducts() {
        System.out.println("Distributor: start distributing products");
        for (int i = 0; i < Product.values().length; i++)  {
            waitForNextDelivery();
            distribute(Product.values()[i]);
        }
    }

    public void distribute(Product product) {
        supermarket.delivery(product);
    }

    private static void waitForNextDelivery() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // ignore exception
        }
    }


}
