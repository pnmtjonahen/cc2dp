package nl.tjonahen.ccdp.exercises.shoppingspree;

import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test the shopping functionality, Customers need a specific product, that can be in stock at the supermarket initially the stock is empty.
 *
 * The distributor is responsible for the supply of products to a supermarket.
 *
 *
 */
class ShoppingTest {
    private List<Customer> customers;

    private Supermarket supermarket;
    private Distributor distributor;

    @BeforeEach
    void setup() {
        customers = List.of(new Customer("Alice", Product.COFFEE)
                , new Customer("Bob", Product.SUGAR)
                , new Customer("James", Product.EGGS)
                , new Customer("Eve", Product.MILK));

        supermarket = new Supermarket();
        distributor = new Distributor(supermarket);
    }

    @Test
    void testGoShopping() throws InterruptedException {

        // start the shoppers async, each customer is using a polling mechanism to find the product it wants in the supermarket
        var shoppingCustomers = allCustomersGoShopping();

        assertTrue(customers.stream().noneMatch(Customer::isHappy));

        // start the distribution of the products, customers wil find these products and become happy
        distributor.distributeAllProducts();

        // stop the async shopping customers
        stopAllShoppingCustomers();

        // wait for all customers to be stopped
        for (Thread thread : shoppingCustomers) {
            thread.join();
        }

        // check if all customers are happy
        assertTrue(customers.stream().allMatch(Customer::isHappy));
    }

    @Test
    void testGoShopping_onlyTheeIsDelivered() throws InterruptedException {

        var shoppingCustomers = allCustomersGoShopping();

        distributor.distribute(Product.THEE);

        stopAllShoppingCustomers();

        for (Thread thread : shoppingCustomers) {
            thread.join();
        }

        assertTrue(customers.stream().noneMatch(Customer::isHappy));
    }


    private void stopAllShoppingCustomers() {
        customers.forEach(customer-> customer.stopShopping());
    }

    private List<Thread> allCustomersGoShopping() {
        return customers.stream()
                .map(customer -> Thread.ofVirtual().start(() -> customer.goShopping(supermarket)))
                .collect(Collectors.toList());
    }

}
